<?php /* Smarty version Smarty-3.1.21, created on 2020-01-10 09:11:42
         compiled from "D:\xampp\htdocs\cscart\design\backend\templates\addons\sd_material\hooks\products\detailed_content.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8671797035e0f02dfcffea1-09754552%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '192f6e911cd6635b2603e3c7659ab64d209f3640' => 
    array (
      0 => 'D:\\xampp\\htdocs\\cscart\\design\\backend\\templates\\addons\\sd_material\\hooks\\products\\detailed_content.pre.tpl',
      1 => 1578636693,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '8671797035e0f02dfcffea1-09754552',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5e0f02dfd243a9_29296600',
  'variables' => 
  array (
    'product_data' => 0,
    'settings' => 0,
    'show_update_for_all' => 0,
    'view_uri' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5e0f02dfd243a9_29296600')) {function content_5e0f02dfd243a9_29296600($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include 'D:/xampp/htdocs/cscart/app/functions/smarty_plugins\\block.hook.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('sd_material.material','available_since','sd_material.comment','full_description','edit_content_on_site'));
?>
                    <div class="control-group">
                        <label for="sd_material" class="control-label"><?php echo $_smarty_tpl->__("sd_material.material");?>
</label>
                        <div class="controls">
                            <input class="input-large" form="form" type="text" name="product_data[material]" id="sd_material" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['material'], ENT_QUOTES, 'UTF-8');?>
" />
                        </div>
                    </div>
					
				    <div class="control-group">
                        <label class="control-label" for="elm_date_avail_holder"><?php echo $_smarty_tpl->__("available_since");?>
:</label>
                        <div class="controls">
						            
                                    <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_date_avail_holder",'date_name'=>"product_data[avail_since]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['product_data']->value['avail_since'])===null||$tmp==='' ? '' : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>
  
                        </div>
                    </div>
					
					<div class="control-group ">
                        <label class="control-label" for="elm_product_full_descr"><?php echo $_smarty_tpl->__("sd_material.comment");?>
:</label>
						
                        <div class="controls">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/update_for_all.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('display'=>$_smarty_tpl->tpl_vars['show_update_for_all']->value,'object_id'=>"full_description",'name'=>"update_all_vendors[full_description]"), 0);?>

                            <textarea id="elm_product_full_descr"
                                      name="product_data[comment1]"
                                      cols="55"
                                      rows="8"
                                      class="cm-wysiwyg input-large"
                                      data-ca-is-block-manager-enabled="<?php echo htmlspecialchars(intval(fn_check_view_permissions("block_manager.block_selection","GET")), ENT_QUOTES, 'UTF-8');?>
"
                            ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['comment1'], ENT_QUOTES, 'UTF-8');?>
</textarea>

                          
                        </div>
                    </div>
					
					<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"products:update_product_full_description")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"products:update_product_full_description"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                    <div class="control-group cm-no-hide-input">
                        <label class="control-label" for="elm_product_full_descr"><?php echo $_smarty_tpl->__("full_description");?>
:</label>
                        <div class="controls">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/update_for_all.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('display'=>$_smarty_tpl->tpl_vars['show_update_for_all']->value,'object_id'=>"full_description",'name'=>"update_all_vendors[full_description]"), 0);?>

                            <textarea id="elm_product_full_descr"
                                      name="product_data[full_description]"
                                      cols="55"
                                      rows="8"
                                      class="cm-wysiwyg input-large"
                                      data-ca-is-block-manager-enabled="<?php echo htmlspecialchars(intval(fn_check_view_permissions("block_manager.block_selection","GET")), ENT_QUOTES, 'UTF-8');?>
"
                            ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['full_description'], ENT_QUOTES, 'UTF-8');?>
</textarea>

                            <?php if ($_smarty_tpl->tpl_vars['view_uri']->value) {?>
                                <?php ob_start();
echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['view_uri']->value), ENT_QUOTES, 'UTF-8');
$_tmp1=ob_get_clean();?><?php ob_start();
if (fn_allowed_for("ULTIMATE")) {?><?php echo "&switch_company_id=";?><?php echo (string)$_smarty_tpl->tpl_vars['product_data']->value['company_id'];?><?php }
$_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>"customization.update_mode?type=live_editor&status=enable&frontend_url=".$_tmp1.$_tmp2,'but_text'=>$_smarty_tpl->__("edit_content_on_site"),'but_role'=>"action",'but_meta'=>"btn-small btn-live-edit cm-post",'but_target'=>"_blank"), 0);?>

                            <?php }?>
                            </div>
                        </div>
                    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"products:update_product_full_description"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>
