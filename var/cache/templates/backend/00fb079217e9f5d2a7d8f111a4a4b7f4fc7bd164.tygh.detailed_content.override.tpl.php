<?php /* Smarty version Smarty-3.1.21, created on 2020-01-17 13:30:00
         compiled from "D:\xampp\htdocs\cscart\design\backend\templates\addons\sd_material\hooks\products\detailed_content.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4434844165e18180c7328e8-35242365%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '00fb079217e9f5d2a7d8f111a4a4b7f4fc7bd164' => 
    array (
      0 => 'D:\\xampp\\htdocs\\cscart\\design\\backend\\templates\\addons\\sd_material\\hooks\\products\\detailed_content.override.tpl',
      1 => 1579256992,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '4434844165e18180c7328e8-35242365',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5e18180c77f795_45007996',
  'variables' => 
  array (
    'product_data' => 0,
    'settings' => 0,
    'show_update_for_all' => 0,
    'view_uri' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5e18180c77f795_45007996')) {function content_5e18180c77f795_45007996($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('sd_material.material','material_available_since','sd_material.comment','edit_content_on_site'));
?>
                    <div class="control-group">
                        <label for="sd_material" class="control-label"><?php echo $_smarty_tpl->__("sd_material.material");?>
</label>
                        <div class="controls">
                            <input class="input-large" form="form" type="text" name="product_data[material]" id="sd_material" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['material'], ENT_QUOTES, 'UTF-8');?>
" />
                        </div>
                    </div>
					
				    <div class="control-group">
                        <label class="control-label" for="elm_date_avail_holder1"><?php echo $_smarty_tpl->__("material_available_since");?>
:</label>
                        <div class="controls">
                                    
                                    <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_date_avail_holder1",'date_name'=>"product_data[material_avail_since]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['product_data']->value['material_avail_since'])===null||$tmp==='' ? '' : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>

                        </div>
                    </div>
					
					<div class="control-group">
                        <label class="control-label" for="elm_product_full_descr"><?php echo $_smarty_tpl->__("sd_material.comment");?>
:</label>
						
                        <div class="controls">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/update_for_all.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('display'=>$_smarty_tpl->tpl_vars['show_update_for_all']->value,'object_id'=>"full_description1",'name'=>"update_all_vendors[full_description1]"), 0);?>
 
                            <textarea id="elm_product_full_descr"
                                      name="product_data[material_comment]"
                                      cols="55"
                                      rows="8"
                                      class="cm-wysiwyg input-large"  
                                      data-ca-is-block-manager-enabled="<?php echo htmlspecialchars(intval(fn_check_view_permissions("block_manager.block_selection","GET")), ENT_QUOTES, 'UTF-8');?>
"
									  ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['comment1'], ENT_QUOTES, 'UTF-8');?>
 
									  </textarea>

                            <?php if ($_smarty_tpl->tpl_vars['view_uri']->value) {?>
                                <?php ob_start();
echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['view_uri']->value), ENT_QUOTES, 'UTF-8');
$_tmp1=ob_get_clean();?><?php ob_start();
if (fn_allowed_for("ULTIMATE")) {?><?php echo "&switch_company_id=";?><?php echo (string)$_smarty_tpl->tpl_vars['product_data']->value['company_id'];?><?php }
$_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>"customization.update_mode?type=live_editor&status=enable&frontend_url=".$_tmp1.$_tmp2,'but_text'=>$_smarty_tpl->__("edit_content_on_site"),'but_role'=>"action",'but_meta'=>"btn-small btn-live-edit cm-post",'but_target'=>"_blank"), 0);?>

                            <?php }?> 
                        </div>
                    </div><?php }} ?>
