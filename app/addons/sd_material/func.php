<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_sd_material_update_product_pre(&$product_data) {

   // fn_print_r('Hook');
    //fn_print_r($product_data);
    //fn_print_r($_data);
    $_data = $product_data;
    //// nachalo izmeneniy
    if (!empty($product_data['material_avail_since'])) {
        $product_data['material_avail_since'] = fn_parse_date($product_data['material_avail_since']);
    }
    //fn_print_die($product_data['material_avail_since']);
}