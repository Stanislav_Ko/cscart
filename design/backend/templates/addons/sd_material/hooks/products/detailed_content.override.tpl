﻿                    <div class="control-group">
                        <label for="sd_material" class="control-label">{__("sd_material.material")}</label>
                        <div class="controls">
                            <input class="input-large" form="form" type="text" name="product_data[material]" id="sd_material" size="55" value="{$product_data.material}" />
                        </div>
                    </div>
					
				    <div class="control-group">
                        <label class="control-label" for="elm_date_avail_holder1">{__("material_available_since")}:</label>
                        <div class="controls">
                                    {** {include file="common/calendar.tpl" date_id="elm_date_avail_holder1" date_name="product_data[avail_since1]" date_val=$product_data.avail_since|default:"" start_year=$settings.Company.company_start_year}  **}
                                    {include file="common/calendar.tpl" date_id="elm_date_avail_holder1" date_name="product_data[material_avail_since]" date_val=$product_data.material_avail_since|default:"" start_year=$settings.Company.company_start_year}
                        </div>
                    </div>
					
					<div class="control-group">
                        <label class="control-label" for="elm_product_full_descr">{__("sd_material.comment")}:</label>
						
                        <div class="controls">
                            {include file="buttons/update_for_all.tpl" display=$show_update_for_all object_id="full_description1" name="update_all_vendors[full_description1]"} 
                            <textarea id="elm_product_full_descr"
                                      name="product_data[material_comment]"
                                      cols="55"
                                      rows="8"
                                      class="cm-wysiwyg input-large"  {* cm-wysiwyg *}
                                      data-ca-is-block-manager-enabled="{fn_check_view_permissions("block_manager.block_selection", "GET")|intval}"
									  >{$product_data.comment1} 
									  </textarea>

                            {if $view_uri}
                                {include
                                    file="buttons/button.tpl"
                                    but_href="customization.update_mode?type=live_editor&status=enable&frontend_url={$view_uri|urlencode}{if "ULTIMATE"|fn_allowed_for}&switch_company_id={$product_data.company_id}{/if}"
                                    but_text=__("edit_content_on_site")
                                    but_role="action"
                                    but_meta="btn-small btn-live-edit cm-post"
                                    but_target="_blank"}
                            {/if} 
                        </div>
                    </div>